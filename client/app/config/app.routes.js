(() => {
    angular
        .module("DMS")
        // .config(dmsConfig);
        // dmsConfig.$inject=["$stateProvider","$urlRouterProvider"]
        // function dmsConfig($stateProvider,$urlRouterProvider){

        // }
        .config(["$stateProvider","$urlRouterProvider",($stateProvider,$urlRouterProvider)=>{
            console.log("enter")
                $stateProvider
                    .state("register",{
                        url:"/register",
                        templateUrl:"app/registration/register.html",
                        controller:"RegCtrl",
                        controllerAs:"ctrl",
                    })
                    .state("search",{
                        url:"/search",
                        templateUrl:"app/search/search.html",
                        controller:"SearchCtrl",
                        controllerAs:"ctrl",
                    })
                    .state("searchDB",{
                        url:"/searchDB",
                        templateUrl:"app/search/searchDB.html",
                        controller:"SearchDBCtrl",
                        controllerAs:"ctrl",
                    })
                    .state("edit",{
                        url:"/edit",
                        templateUrl:"app/edit/edit.html",
                        controller:"EditCtrl",
                        controllerAs:"ctrl",
                    })
                    .state("editWithParams",{
                        url:"/edit/:dept_no",
                        templateUrl:"app/edit/edit.html",
                        controller:"EditCtrl",
                        controllerAs:"ctrl",
                    })
                    $urlRouterProvider.otherwise("/register");
        }]);


})();
// Handles API routes

module.exports = function(app, db) {
  var Department = require('./api/department.controller')(db);
  var Employee = require('./api/employee.controller.js')(db);

  // Create new department
  app.post("/api/departments", Department.create);

  // Retrieve static department records
  app.get("/api/departments", Department.retrieve);

  // Retrieve all department records that match query string passed. 
  app.get("/api/departmentsDB", Department.retrieveDB);

  // Retrieve department records (including manager info) that match query string passed. 
  app.get("/api/deptManagers", Department.retrieveDeptManager);

  app.get("/api/departments/:dept_no",Department.retrieveByDept_no)

  //Retrieve employee records
  app.get("/api/employees", Employee.retrieveEmp);


};

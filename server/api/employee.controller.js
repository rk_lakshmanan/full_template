var retrieveEmp = function (db) {
    return function (req, res) {
        // There are cases where sequelize is not robust enough to handle certain SQL queries. Sequelize has the mechanism
        // that allows queries to be written in native SQL. In this case, we're writing in SQL because currently Sequelize
        // doesn't have an equivalent for WHERE NOT EXISTS

        // Explanation of SQL statement
        // 1. SELECT - SELECT specifies that this is a read/retrieve command
        // 2. emp_no, ... - identifies the columns to return; use * to return all columns
        // 3. FROM employees - specifies the table to read data from
        // 4. e - specifies a shorthand for the preceding table; this is optional
        // 5. WHERE - signals that subsequent statement is a condition
        // 6. NOT EXISTS - a subquery specifying that for a record from mainTable to be selected, it must not exist in
        // subTable; in our case, we use this subquery to ensure that we get only those employees that are currently not
        // managers
        // 7. (SELECT ...) - this is the subquery where we apply the NOT EXISTS clause; composition is explained as above;
        // the WHERE clause checks whether a record from the employee table exists in the dept_manager table
        // 8. LIMIT - limits the number of records returned; we limit to 100 because employees table is too big
        
        db.sequelize
            .query("SELECT emp_no, first_name, last_name " +
            "FROM employees e " +
            "WHERE NOT EXISTS " +
            "(SELECT * " +
            "FROM dept_manager dm " +
            "WHERE dm.emp_no = e.emp_no )" +
            "LIMIT 100; "
            )
            // this .spread() handles successful native query operation
            // we use .spread instead of .then so as to separate metadata from the emplooyee records
            .spread(function (employees) {
                res
                    .status(200)
                    .json(employees);
            })
            // this .catch() handles erroneous native query operation
            .catch(function (err) {
                res
                    .status(500)
                    .json(err);
            });
    }
};



// Export route handlers
module.exports = function (db) {
    return {
        retrieveEmp: retrieveEmp(db),
        // retrieveDB: retrieveDB(db),
        // retrieveDeptManager: retrieveDeptManager(db),
        // create: create(db),
    }
};

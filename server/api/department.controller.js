// Handles department DB queries

// Retrieve from local memory
var retrieve = function (db) {
  return function (req, res) {
    var departments = [
      {
        deptNo: 1001,
        deptName: 'Admin'
      }, {
        deptNo: 1002,
        deptName: 'Finance'
      }, {
        deptNo: 1003,
        deptName: 'Sales'
      }, {
        deptNo: 1004,
        deptName: 'HR'
      }, {
        deptNo: 1005,
        deptName: 'Staff'
      }, {
        deptNo: 1006,
        deptName: 'Customer Care'
      }, {
        deptNo: 1007,
        deptName: 'Support'
      }
    ];

    res
      .status(200)
      .json(departments);
  }
};

// Retrieve from DB
var retrieveDB = function (db) {
  return function (req, res) {
    console.log("in retrieveDB")
    if (!req.query.searchString) req.query.searchString = '';

    console.log("SEARCHSTRING")
    console.log(req.query.searchString);
    db.Department
      .findAll({
        where: {
          $or: [
            { dept_name: { $like: "%" + req.query.searchString + "%" } },
            { dept_no: { $like: "%" + req.query.searchString + "%" } }
          ]
        }
      })
      .then(function (departments) {
        res
          .status(200)
          .json(departments);
      })
      .catch(function (err) {
        console.log("departmentsDB error clause: " + err);
        res
          .status(500)
          .json(err);
      });
  }
};

// Retrieve department managers info
var retrieveDeptManager = function (db) {
  return function (req, res) {
    if (!req.query.searchString) req.query.searchString = '';

    db.Department
      .findAll({
        where: {
          $or: [
            { dept_name: { $like: "%" + req.query.searchString + "%" } },
            { dept_no: { $like: "%" + req.query.searchString + "%" } }
          ]
        },
        include: [{
          model: db.DeptManager,
          order: [["to_date", "DESC"]],
          limit: 1,
          include: [db.Employee]
        }],
      })
      .then(function (departments) {
        res
          .status(200)
          .json(departments);
      })
      .catch(function (err) {
        res
          .status(500)
          .json(err);
      });
  }
};

// Create a new departmnet
var create = function (db) {
  return function (req, res) {
    //Simple creatte
    // console.log('\nData Submitted');
    // console.log('Dept No: ' + req.body.dept.id);
    // console.log('Dept Name: ' + req.body.dept.name);

    // db.Department
    //   .create({
    //     dept_no: req.body.dept.id,
    //     dept_name: req.body.dept.name
    //   })
    //   .then(function(department) {
    //     console.log(department.get({ plain: true }));
    //     res
    //       .status(200)
    //       .json(department);
    //   })
    //   .catch(function(err) {
    //     console.log("error: " + err);
    //     res
    //       .status(500)
    //       .json(err);
    //   });

    // TODO: 6.2 Create a transaction (atomic operation) where department record is created only when an equivalent
    // TODO: 6.2 dept_manager record is created
    // This demonstrates how transactions (i.e., atomic operations) are performed. In this sample, a department record
    // is created only when an equivalent dept_manager record is created
    // sequelize.transaction is a managed transaction (i.e., handles rollback automatically)
    db.sequelize
      .transaction(function (t) {
        return db.Department
          .create(
          {
            dept_no: req.body.dept.id
            , dept_name: req.body.dept.name
          }
          , { transaction: t })
          .then(function (department) {
            console.log("inner result " + JSON.stringify(department))
            return db.DeptManager
              .create(
              {
                dept_no: req.body.dept.id
                , emp_no: req.body.dept.manager
                , from_date: req.body.dept.from_date
                , to_date: req.body.dept.to_date
              }
              , { transaction: t });
          });
      })
      .then(function (results) {
        res
          .status(200)
          .json(results);
      })
      .catch(function (err) {
        console.log(err);
        // console.log("outer error: " + JSON.stringify(err));
        res
          .status(500)
          .json(err);
      });
  }
};

var retrieveByDept_no = function (db) {
  return function (req, res) {
    var where = {};
    if (req.params.dept_no) {
      where.dept_no = req.params.dept_no
    }

    // We use findOne because we know (by looking at the database schema) that dept_no is the primary key and
    // is therefore unique. We cannot use findById because findById does not support eager loading
    db.Department
      .findOne({
        where: where
        // What Include attribute does: Join two or more tables. In this instance:
        // 1. For every Department record that matches the where condition, the include attribute returns
        // ALL employees that have served as managers of said Department
        // 2. model attribute specifies which model to join with primary model
        // 3. order attribute specifies that the list of Managers be ordered from latest to earliest manager
        // 4. limit attribute specifies that only 1 record (in this case the latest manager) should be returned
        , include: [{
          model: db.DeptManager
          , order: [["to_date", "DESC"]]
          , limit: 1
          // We include the Employee model to get the manager's name
          , include: [db.Employee]
        }]
      })
      // this .then() handles successful findAll operation
      // in this example, findAll() used the callback function to return departments
      // we named it departments, but this object also contains info about the
      // latest manager of that department
      .then(function (departments) {
        console.log("-- GET /api/departments/:dept_no findOne then() result \n " + JSON.stringify(departments));
        res.json(departments);
      })
      // this .catch() handles erroneous findAll operation
      .catch(function (err) {
        console.log(err);
        // console.log("-- GET /api/departments/:dept_no findOne catch() \n " + JSON.stringify(err));
        res
          .status(500)
          .json({ error: true });
      });
  }
}
// Export route handlers
module.exports = function (db) {
  return {
    retrieve: retrieve(db),
    retrieveDB: retrieveDB(db),
    retrieveDeptManager: retrieveDeptManager(db),
    create: create(db),
    retrieveByDept_no: retrieveByDept_no(db),
  }
};
